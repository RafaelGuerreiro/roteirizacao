package com.mapfre.roteirizacao.configuration;

import static java.io.File.separator;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import com.mapfre.roteirizacao.configuration.Configuration;
import com.mapfre.roteirizacao.configuration.Configurations;

public class ConfigurationsTest {

	@Test
	public void configuration_can_be_empty() {
		Configurations configurations = new Configurations();
		assertTrue(configurations.isEmpty());
	}

	@Test
	public void should_add_key_as_parameter() {
		final String key = "AIzaSyD_wOKaknpbm02e0jBMyzUgulX-dGBVCDs";

		Configurations configurations = new Configurations("maps_key=" + key, "other_key=this is other key");
		assertEquals(key, configurations.getMapsKey());
		assertEquals("this is other key", configurations.getPropertyValue("other_key"));
		assertEquals("", configurations.getPropertyValue("this_key_doesnt_exists"));

		assertEquals(2, configurations.size());
		assertFalse(configurations.isEmpty());
	}

	@Test
	public void should_add_and_format_target_path_correctly() {
		final String path = "/Users/my-user/my folder/test";
		final String otherPath = "/Users/other-user/other folder/afile.txt";

		Configurations configurations = new Configurations("log_directory=" + path, "other_path=" + otherPath);
		assertEquals(path.replace("/", separator), configurations.getLogDirectory());
		assertEquals(otherPath.replace("/", separator), configurations.getPropertyPath("other_path"));
		assertEquals("", configurations.getPropertyPath("this_key_doesnt_exists"));

		assertEquals(2, configurations.size());
		assertFalse(configurations.isEmpty());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void configurations_should_be_immutable() {
		Configurations configurations = new Configurations("key=123123", "key2=456456");
		Collection<Configuration> collection = configurations.get();

		collection.add(Configuration.build("key2=456456"));
	}
}