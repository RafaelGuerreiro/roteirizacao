package com.mapfre.roteirizacao.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.mapfre.roteirizacao.configuration.Configuration;

public class ConfigurationTest {

	@Test
	public void to_string_should_be_the_same_as_pair() {
		String pair = "key=this is a valid key";
		Configuration configuration = Configuration.build(pair);

		assertNotNull(configuration);
		assertEquals(pair, configuration.toString());
		assertEquals("key", configuration.getKey());
		assertEquals("this is a valid key", configuration.getValue());
		assertFalse(configuration.isKeyOnlyMode());
	}

	@Test
	public void should_lowercase_keys() {
		String pair = "THIS_is_A_vaLID_KEY=this is a valid key";
		Configuration configuration = Configuration.build(pair);

		assertNotNull(configuration);
		assertEquals(pair.toLowerCase(), configuration.toString());
		assertEquals("this_is_a_valid_key", configuration.getKey());
		assertEquals("this is a valid key", configuration.getValue());
		assertFalse(configuration.isKeyOnlyMode());
	}

	@Test
	public void only_the_first_equal_sign_should_be_used() {
		String pair = "THIS_is_A_vaLID_KEY=this is a = valid key";
		Configuration configuration = Configuration.build(pair);

		assertNotNull(configuration);
		assertEquals(pair.toLowerCase(), configuration.toString());
		assertEquals("this_is_a_valid_key", configuration.getKey());
		assertEquals("this is a = valid key", configuration.getValue());
		assertFalse(configuration.isKeyOnlyMode());
	}

	@Test
	public void space_is_a_valid_value() {
		String pair = "THIS_is_A_vaLID_KEY= ";
		Configuration configuration = Configuration.build(pair);

		assertNotNull(configuration);
		assertEquals(pair.toLowerCase(), configuration.toString());
		assertEquals("this_is_a_valid_key", configuration.getKey());
		assertEquals(" ", configuration.getValue());
		assertFalse(configuration.isKeyOnlyMode());
	}

	@Test
	public void should_return_null_when_pair_is_null() {
		assertNull(Configuration.build(null));
	}

	@Test
	public void should_return_null_when_pair_is_invalid() {
		assertNull(Configuration.build(""));
		assertNull(Configuration.build(" key=this is a invalid key"));
		assertNull(Configuration.build("key: test"));
		assertNull(Configuration.build("key = this is a invalid key"));
		assertNull(Configuration.build("key="));
	}

	@Test
	public void should_return_a_key_only_mode() {
		Configuration configuration = Configuration.asKey("ThisIs_a_VALID_key");

		assertNotNull(configuration);
		assertEquals("THIS IS A KEY ONLY INSTANCE. key=thisis_a_valid_key", configuration.toString());
		assertTrue(configuration.isKeyOnlyMode());
	}

	@Test
	public void key_only_mode_can_be_compared() {
		Configuration keyOnlyConfiguration = Configuration.asKey("ThisIs_a_VALID_key");

		assertNotNull(keyOnlyConfiguration);
		assertEquals("THIS IS A KEY ONLY INSTANCE. key=thisis_a_valid_key", keyOnlyConfiguration.toString());
		assertTrue(keyOnlyConfiguration.isKeyOnlyMode());

		Configuration configuration = Configuration.build("ThisIs_a_VALID_key=This is a valid value");
		assertNotNull(configuration);
		assertEquals("thisis_a_valid_key=This is a valid value", configuration.toString());
		assertFalse(configuration.isKeyOnlyMode());

		assertEquals(configuration, keyOnlyConfiguration);
		assertEquals(configuration.hashCode(), keyOnlyConfiguration.hashCode());
	}

	@Test(expected = IllegalStateException.class)
	public void cannot_invoke_get_key_from_key_only_configuration() {
		Configuration keyOnlyConfiguration = Configuration.asKey("ThisIs_a_VALID_key");

		assertNotNull(keyOnlyConfiguration);
		assertEquals("THIS IS A KEY ONLY INSTANCE. key=thisis_a_valid_key", keyOnlyConfiguration.toString());
		assertTrue(keyOnlyConfiguration.isKeyOnlyMode());

		keyOnlyConfiguration.getKey();
	}

	@Test(expected = IllegalStateException.class)
	public void cannot_invoke_get_value_from_key_only_configuration() {
		Configuration keyOnlyConfiguration = Configuration.asKey("ThisIs_a_VALID_key");

		assertNotNull(keyOnlyConfiguration);
		assertEquals("THIS IS A KEY ONLY INSTANCE. key=thisis_a_valid_key", keyOnlyConfiguration.toString());
		assertTrue(keyOnlyConfiguration.isKeyOnlyMode());

		keyOnlyConfiguration.getValue();
	}
}
