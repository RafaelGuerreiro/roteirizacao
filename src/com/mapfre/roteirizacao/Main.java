package com.mapfre.roteirizacao;

import java.io.IOException;

import com.mapfre.roteirizacao.configuration.Configurations;
import com.mapfre.roteirizacao.log.Logger;

public class Main {
	public static void main(String[] args) throws IOException {
		// maps_key=AIzaSyD_wOKaknpbm02e0jBMyzUgulX-dGBVCDs
		Configurations configurations = new Configurations(args);
		Logger logger = new Logger(configurations.getLogDirectory());

		configurations.logMissingConfigurations(logger);
	}
}