package com.mapfre.roteirizacao.configuration;

public class Configuration implements Comparable<Configuration> {

	private final String key;
	private final String value;

	public static Configuration asKey(String key) {
		if (key == null)
			return null;

		return new Configuration(key.trim(), null);
	}

	public static Configuration build(String pair) {
		if (pair == null || !pair.matches("^\\w+=.*?$"))
			return null;

		final String[] split = pair.split("=", 2);

		if (split.length != 2)
			return null;

		final String key = split[0];
		final String value = split[1];

		if (isEmpty(key) || isEmpty(value))
			return null;

		return new Configuration(key, value);
	}

	private static boolean isEmpty(String string) {
		if (string == null || string.isEmpty())
			return true;

		return false;
	}

	private Configuration(String key, String value) {
		this.key = key.toLowerCase();
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Configuration other = (Configuration) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}

	@Override
	public String toString() {
		if (isKeyOnlyMode())
			return new StringBuilder("THIS IS A KEY ONLY INSTANCE. key=").append(key).toString();

		return new StringBuilder(key).append('=').append(value).toString();
	}

	@Override
	public int compareTo(Configuration o) {
		if (o == null)
			return 1;

		return key.compareTo(o.getKey());
	}

	public boolean isKeyOnlyMode() {
		return value == null;
	}

	public String getKey() {
		if (isKeyOnlyMode())
			throw new IllegalStateException("You shouldn't use this instance.");

		return key;
	}

	public String getValue() {
		if (isKeyOnlyMode())
			throw new IllegalStateException("You shouldn't use this instance.");

		return value;
	}
}