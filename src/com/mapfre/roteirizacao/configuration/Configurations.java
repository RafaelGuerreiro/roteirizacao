package com.mapfre.roteirizacao.configuration;

import static com.mapfre.roteirizacao.configuration.ConfigurationType.LOG_DIRECTORY;
import static com.mapfre.roteirizacao.configuration.ConfigurationType.MAPS_KEY;
import static com.mapfre.roteirizacao.configuration.ConfigurationType.MAPS_URL;
import static com.mapfre.roteirizacao.configuration.ConfigurationType.RESULT_FILE;
import static com.mapfre.roteirizacao.configuration.ConfigurationType.SOURCE_FILE;
import static com.mapfre.roteirizacao.configuration.ConfigurationType.getKey;
import static com.mapfre.roteirizacao.configuration.ConfigurationType.values;
import static java.io.File.separator;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableSet;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.mapfre.roteirizacao.log.Logger;

public class Configurations implements Iterable<Configuration> {

	private final Map<String, Configuration> properties = new HashMap<String, Configuration>();

	public Configurations(String... configurations) {
		this.properties.putAll(build(configurations));
	}

	private Map<String, Configuration> build(String... configurations) {
		Map<String, Configuration> properties = new HashMap<String, Configuration>();

		for (String pair : configurations) {
			Configuration configuration = Configuration.build(pair);

			if (configuration != null && !properties.containsKey(configuration.getKey()))
				properties.put(configuration.getKey(), configuration);
		}

		return properties;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((properties == null) ? 0 : properties.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Configurations other = (Configurations) obj;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (!properties.equals(other.properties))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return new StringBuilder("Configurations => ").append(get()).toString();
	}

	public String getMapsKey() {
		return getPropertyValue(MAPS_KEY);
	}

	public String getLogDirectory() {
		return getPropertyPath(LOG_DIRECTORY);
	}

	public String getMapsUrl() {
		return getPropertyValue(MAPS_URL);
	}

	public String getResultFile() {
		return getPropertyPath(RESULT_FILE);
	}

	public String getSourceFile() {
		return getPropertyPath(SOURCE_FILE);
	}

	public boolean contains(ConfigurationType key) {
		return contains(getKey(key));
	}

	public boolean contains(Configuration key) {
		if (key == null)
			return false;

		return properties.containsKey(key.getKey());
	}

	public boolean contains(String key) {
		return properties.containsKey(key);
	}

	public String getPropertyPath(ConfigurationType type) {
		return getPropertyPath(getKey(type));
	}

	public String getPropertyPath(String key) {
		return getPropertyValue(key).replace("/", separator);
	}

	public String getPropertyValue(ConfigurationType type) {
		return getPropertyValue(getKey(type));
	}

	public String getPropertyValue(String key) {
		Configuration property = getProperty(key);

		if (property != null)
			return property.getValue();

		return "";
	}

	public Configuration getProperty(ConfigurationType type) {
		return getProperty(getKey(type));
	}

	public Configuration getProperty(String key) {
		if (key == null)
			return null;

		return properties.get(key);
	}

	public Collection<Configuration> get() {
		return unmodifiableCollection(properties.values());
	}

	public Set<String> getKeys() {
		return unmodifiableSet(properties.keySet());
	}

	@Override
	public Iterator<Configuration> iterator() {
		return get().iterator();
	}

	public int size() {
		return properties.size();
	}

	public boolean isEmpty() {
		return properties.isEmpty();
	}

	public void logMissingConfigurations(Logger logger) {
		Set<ConfigurationType> missing = getMissingConfigurations();

		if (missing.isEmpty())
			return;

		logger.fatal("Unable to execute program because there are missing configurations =>\n\t%s", missing.toString());
	}

	private Set<ConfigurationType> getMissingConfigurations() {
		final Set<ConfigurationType> missing = new HashSet<ConfigurationType>();

		for (ConfigurationType type : values())
			if (!contains(type))
				missing.add(type);

		return missing;
	}
}