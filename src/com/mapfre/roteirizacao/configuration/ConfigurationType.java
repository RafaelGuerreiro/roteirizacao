package com.mapfre.roteirizacao.configuration;

public enum ConfigurationType {
	MAPS_URL, MAPS_KEY, LOG_DIRECTORY, SOURCE_FILE, RESULT_FILE;

	public static String getKey(ConfigurationType type) {
		if (type == null)
			return null;

		return type.toKey();
	}

	public String toKey() {
		return name().toLowerCase();
	}
}