package com.mapfre.roteirizacao.log;

import static java.io.File.separator;
import static java.lang.String.format;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.joda.time.DateTime;

public class Logger {

	private final File log;

	public Logger(String directory) {
		try {
			DateTime now = DateTime.now();
			String filePath = format("log_%s_%s.log", now.toString("yyyyMMdd"), now.toString("hh24mmss"));

			if (directory != null && !directory.isEmpty()) {
				if (!directory.endsWith(separator))
					directory += separator;

				filePath = directory + filePath;
			}

			log = new File(filePath);
			log.createNewFile();
		} catch (IOException e) {
			throw new RuntimeException("Unable to start logger.", e);
		}
	}

	public void fatal(String message, Object... parameters) {
		String log = log("FATAL", message, parameters);
		throw new RuntimeException("Stopping program => " + log);
	}

	public void severe(String message, Object... parameters) {
		log("SEVERE", message, parameters);
	}

	public void danger(String message, Object... parameters) {
		log("DANGER", message, parameters);
	}

	public void warn(String message, Object... parameters) {
		log("WARN", message, parameters);
	}

	public void info(String message, Object... parameters) {
		log("INFO", message, parameters);
	}

	public void debug(String message, Object... parameters) {
		log("DEBUG", message, parameters);
	}

	public void trace(String message, Object... parameters) {
		log("TRACE", message, parameters);
	}

	private String log(String type, String message, Object... parameters) {
		if (message == null)
			return "";
		String full = format("%s => %s", type, format(message, parameters));

		try {
			FileOutputStream fos = null;

			try {

				fos = new FileOutputStream(log);
				fos.write(full.getBytes());
			} catch (IOException e) {
				throw e;
			} finally {
				if (fos != null)
					fos.close();
			}
		} catch (IOException e) {
			System.out.println("SEVERE => Unable to log message: " + full);
		}

		return full;
	}
}